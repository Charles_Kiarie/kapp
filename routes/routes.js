const path = require('path');
const express = require('express');

const router = express.Router();

const indexController = require('../controllers/index');

router.get('/',indexController.getIndex);
router.get('/contacts', indexController.getContact);
router.post('/contacts', indexController.postContact);
router.get('/thanks', indexController.getThanks);

module.exports = router;