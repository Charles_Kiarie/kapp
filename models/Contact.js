const getDb = require('../util/database').getDb;

class Contact {
	constructor(email, mobileNumber) {
		this.email = email;
		this.mobileNumber = mobileNumber;
	}

	save() {
		const db = getDb();
		return db.collection('contacts')
		.insertOne(this)
		.then(result => {
			console.log(result);
		})
		.catch(error => {
			console.log(error)
		});
	}

	findAll() {
		const db = getDb();
		return db.collection('contacts')
		.find()
		.toArray()
		.then(contacts => {
			console.log(contacts);
			return contacts;
		})
		.catch(error => {
			console.log(error)
		});
	}
}

module.exports = Contact;