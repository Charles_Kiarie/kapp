eventListeners();

function eventListeners() {
	document.addEventListener('DOMContentLoaded', init);
}

function init() {
	document.addEventListener('mouseout', displayModal);
}

function displayModal(e) {
	if (e.clientY <= 1) {
		$('#sub-modal').modal('show');
	}
}

