const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const helmet = require('helmet');

const app = express();
const PORT = process.env.PORT || 3000;

const routes = require('./routes/routes');
const errorController = require('./controllers/error');

const mongoConnect = require('./util/database').mongoConnect;

app.use(helmet());
app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'ejs');
app.set('views', 'views');
app.use(bodyParser.urlencoded({ extended:false }));

app.use(routes);
app.use(errorController.get404);

mongoConnect(client => {
	//console.log(client);
	app.listen(PORT);
});


