const path = require('path');
const nodemailer = require('nodemailer');
const Contact = require('../models/Contact');

exports.getIndex = (req, res) => {
	 res.sendFile("index.html", {root: path.join(__dirname, '../views')});
};

exports.getThanks = (req, res) => {
	 res.sendFile("thanks.html", {root: path.join(__dirname, '../views')});
};

exports.postContact = (req, res) => {
	const email = req.body.email;
	const mobileNumber = req.body.mobileNumber;
	sendEmail(email, mobileNumber);
	const contact = new Contact(email, mobileNumber);
	contact.save()
	.then(result => {
		res.redirect('/thanks');
	})
	.catch(error => {
		console.log(error);
		res.redirect('/index');
	});
};

exports.getContact = (req, res) => {
	const contact = new Contact();
	contact.findAll()
	.then( contacts => {
		res.render('contacts', {
			contacts: contacts
		});
	})
	.catch(error => {
		console.log(error);
	})
};

const sendEmail = (email, mobileNumber) => {
	var transporter = nodemailer.createTransport({
		service: 'Gmail',
		auth: {
			user: process.env.EMAIL_USER,
			pass: process.env.EMAIL_PWD
		}
	});

	var mailOptions = {
		from: 'Kapp <kiariecharles77@gmail.com>',
		to: 'kiariecharles77@gmail.com',
		subject: 'Somebody signed up',
		text: `I want to get notified when Kapp launches at ${email} my number is ${mobileNumber}`,
		html: `<p>I want to get notified when Kapp launches at <strong>${email}</strong> my number is <strong>${mobileNumber}</strong></p>`
	};

	transporter.sendMail(mailOptions, function(error, info){
		if(error){
			console.log(error);
		} else {
			console.log('Message Sent: '+info.response);
		}
	});
};